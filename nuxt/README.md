# Nuxt 101

- [Nuxt](https://nuxt.com/docs/getting-started/introduction) official documentation

## required extensions

- [Vuetify](https://marketplace.visualstudio.com/items?itemName=vuetifyjs.vuetify-vscode)
- [Vue](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

## installation

### create project

Run following command

```bash
$ npx nuxi@latest init <project-name>
```

For package manager select `npm` or `yarn` (up to you)

![Packaging](./package.png)

Firstly select no for git if you do not want to use git

![GIt](./git.png)

### running project

Go to project directory

```bash
$ cd <project-name>
```

Run project in development mode

```bash
# using yarn
$ yarn dev
# using npm
$ npm run dev
```

## vuetify installation

- [vuetify](https://vuetifyjs.com/en/getting-started/installation/#using-nuxt-3) documentation

### install to nuxt

install dependencies

```bash
# using yarn
$ yarn add -D vuetify vite-plugin-vuetify
$ yarn add @mdi/font

# using npm
$ npm i -D vuetify vite-plugin-vuetify
$ npm i @mdi/font
```

Set up vuetify in `~/nuxt.config.ts`

```ts
export default defineNuxtConfig({
  devtools: { enabled: true },

  // copy code below
  build: {
    transpile: ["vuetify"],
  },
  modules: [
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        // @ts-expect-error
        config.plugins.push(vuetify({ autoImport: true }));
      });
    },
    //...
  ],
  vite: {
    vue: {
      template: {
        transformAssetUrls,
      },
    },
  },
});
```

Then, create plugins for vuetify configuration in `~/plugins/vuetify.ts`

```ts
import "@mdi/font/css/materialdesignicons.css";

import "vuetify/styles";
import { createVuetify } from "vuetify";

export default defineNuxtPlugin((app) => {
  const vuetify = createVuetify({
    // ... your configuration
  });
  app.vueApp.use(vuetify);
});
```

## nuxt set up for vuetify

Change template in `~/app.vue`

```vue
<template>
  <div>
    <NuxtLayout>
      <NuxtPage />
    </NuxtLayout>
  </div>
</template>
```

Then create folder `~/layouts` and create file `~/layouts/default.vue`

```vue
<template>
  <v-layout>
    <v-main>
      <slot></slot>
    </v-main>
  </v-layout>
</template>
```

Finally, create folder `~/pages` and create file `~/pages/index.vue`

```vue
<script setup lang="ts">
  const name = useState("name");
</script>

<template>
  <v-container>
    <div>Username: {{ name }}</div>
  </v-container>
</template>
```


