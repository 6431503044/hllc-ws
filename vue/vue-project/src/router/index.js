import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ProfileView from "../views/ProfileView.vue";
import ActivityView from "@/views/ActivityView.vue";
import ActivityIdView from "@/views/ActivityIdView.vue";
import ActivityHelloView from "@/views/ActivityHelloView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/profile",
      name: "profile",
      component: ProfileView,
    },
    {
      path: "/activities",
      name: "Activity",
      component: ActivityView,
      children: [
        {
          path: "nested",
          name: "nested activities",
          component: ActivityIdView,
        },
        {
          path: ":hello",
          name: "hello activities",
          component: ActivityHelloView,
        },
      ],
    },
  ],
});

export default router;
