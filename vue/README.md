# Vue 101

## The required installation

- [Node.js](https://nodejs.org/en/download/prebuilt-installer)

![Node installer](./node-install.png)

to make sure installation is successful

```bash
$ node -v
```

The result must be 18.X.X

![Node version](./node.png)

## Vue installation

- [Vue documentation](https://vuejs.org/guide/introduction.html)

Create a new vue project

```bash
$ npm create vue@latest
```

Then select following options

![Create vue options](./create.png)

## Running the project

We recommend to run the project using yarn

```bash
$ npm install -g yarn
```

Go to the project directory

```bash
$ cd project-name
```

Install dependencies

```bash
$ yarn
```

Start the project

```bash
$ yarn dev
```

The result must be

![Dev](./dev.png)
